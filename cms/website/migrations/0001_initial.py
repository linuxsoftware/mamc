# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0019_verbose_names_cleanup'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlainPage',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, primary_key=True, auto_created=True, to='wagtailcore.Page', serialize=False)),
                ('body', wagtail.wagtailcore.fields.RichTextField(default='', help_text='An area of text for whatever you like', blank=True)),
            ],
            options={
                'verbose_name': 'Plain Page',
            },
            bases=('wagtailcore.page',),
        ),
    ]
