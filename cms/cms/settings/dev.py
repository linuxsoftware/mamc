from .base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

#TEMPLATE_DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 't(h1!yc!iaqi#+9_8rn9q6c#o)e)g9v3v35aj8-xflp+_k44pr'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# A sample logging configuration.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'filters': {
#         'require_debug_false': {
#             '()': 'django.utils.log.RequireDebugFalse'
#         }
#     },
#     'handlers': {
#          'file': {
#             'level': 'INFO',
#             'class': 'logging.FileHandler',
#             'filename': '/home/djm/djangoerr.log',
#         },
#        'mail_admins': {
#             'level': 'ERROR',
#             'filters': ['require_debug_false'],
#             'class': 'django.utils.log.AdminEmailHandler'
#         }
#     },
#     'loggers': {
#         'django.request': {
#             'handlers': ['file'],
#             'level': 'INFO',
#             'propagate': True,
#         },
#     },
# }
#



try:
    from .local import *
except ImportError:
    pass
