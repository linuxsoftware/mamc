# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import modelcluster.fields
import wagtail.wagtailcore.fields
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0019_verbose_names_cleanup'),
        ('wagtailimages', '0008_image_created_at_index'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomePage',
            fields=[
                ('page_ptr', models.OneToOneField(to='wagtailcore.Page', parent_link=True, serialize=False, primary_key=True, auto_created=True)),
                ('welcome', wagtail.wagtailcore.fields.RichTextField(blank=True, default='', help_text='A short introductory message')),
                ('body', wagtail.wagtailcore.fields.RichTextField(blank=True, default='', help_text='An area of text for whatever you like')),
                ('banner_image', models.ForeignKey(help_text="A big wide image (at least 1440x650px) to grab the viewer's attention", on_delete=django.db.models.deletion.SET_NULL, null=True, to='wagtailimages.Image', blank=True, related_name='+')),
            ],
            options={
                'verbose_name': 'Homepage',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='HomePageHighlight',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, null=True, editable=False)),
                ('title', models.CharField(blank=True, max_length=80, verbose_name='Title')),
                ('blurb', wagtail.wagtailcore.fields.RichTextField(blank=True, default='')),
                ('homepage', modelcluster.fields.ParentalKey(related_name='highlights', to='home.HomePage')),
                ('image', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, null=True, to='wagtailimages.Image', blank=True, related_name='+')),
                ('page', models.ForeignKey(null=True, to='wagtailcore.Page', blank=True, related_name='+')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
    ]
